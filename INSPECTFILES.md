# Inspect files of my angular app:

## .browserlistrc:

This file is used by the build system to adjust CSS and JS output to support the specified browsers defined at the bottom of the file.

## .editorconfig:

Configuration du code pour les differents developeurs qui travaillent sur le meme projet, in order to maintain a consistant code.

### Inspect elements

if `root = true` : don't apply the changes from other config files.

if `insert_final_newline = true` : keep the last line in files always empty.

if `trim_trailing_whitespace = true` : delete white spaces.

## .gitignore

Tells git to ignore specific files.

## karma.conf.js

Karma , it is a task runner for our tests. The config file sets the startup file, the testing framework, as well as teh browser..

## angular.json

Contains configuration defaults for all projects in the workspace, including configuration options for build, serve, and test tools that the CLI uses,

## tsconfig.app.json and tsconfig.json

These files containes the configuration the `tsconfig.app.json` is specific for the Angular App and the `tsconfig.json` is global and can be deleted.

## the src file

### the app file

- app.component.css

File for the style of the components defined at the typescript file below

- app.component.html

File or template for of the elements defined at the typescript and their display

- app.component.ts

File where we define the components and classes and export the result to the html file above and apply the css stylesheet by calling the css file above

- app.module.ts

This file as the different components that we created and the future services, controllers, filters etc

### the asset file

The file where we usually put our media stuff: videos, images, icons..
And css and js libraries or files as well

### main.ts

This file is a simple script-file created by the angular compiler based on the config read from angular.json

### polyfills.ts

Makes my app compatible with browsers Angular CLI brought through the `zone.js` file

### index.html

This is the main html page of the application, where all the js and css files are added by the CLI

## Inspecting part two : two lazy loading feature modules

The lazy loading is a technique that loads modules, components only when needed.., it helps for the better performance of the application and to reduce the initial bundle size of our files. The inital page unlike react loads faster.

The command `ng generate module One --route One --module app.model` allows us to create the feature module, and it's routing.

The two commands got us two folders one and two:

- Each has its component and HTML and CSS files and their module file where the component and its route is declered.

## Part three

Even putting the router links outside the `<router-outlet></router-outlet>`, the app worked just fine and the result was as expected.

## Part four

Create a module `Shared` [*1] that declares and exports `Reusable` component [*2]
[*1] : ng g module Shared
[*2] : ng g component Reusable --module Shared

What I learned :

I fixed my project by adding the aoo-routing manually in order to seperate the routes declaration from the components, directives pipes and services declaration

- The `CommonModule` contains the directives ngIf, ngFor..
- The `BrowserModule` is used to render in the browser.

## Part five:

Observable are maybe like axios 3la mafhemt they do the asynch await work. Let's check more.
They provide support for passing messages between parts of the app, yes Angular makes use of observables to handle some asynchron operations and event handling.

Thsi observable are used in a service file, and then we inject for each component the specific service needed : here comes the term of dependency injection.

The observer has subjects, objects that has observers, notified on state changes.

We define a function for publishing values, it is executed when a consumer subscribes to it. Then the subscribed consuer keep getting notifications until the function completes or the consumer is unsub.

A subject takes a notification from a ource and send it to other detinations.

In the `state.service.ts` file we have:

- `@Injectable` : It is the decorator that defines a service file or service class more specifically. It was called Injecteable to tell us the services available in the class are to be injected.
- `providedIn` : is the parameter that tells us wich ModuleInjector to us, in our case it's root
- `MouleInjector` : injector that will create an instance of our dependency and provide it as a contructor parameter for our components, it provides the `@Injectable providedIn` property as well as the `@NgModule providers` that is not preferable for optimization reasons.

### Part six:

Call a module in a module that calls a component.
Display the Reusable's content in the App view.

### Part seven : checkbox

Create a simple checkbox in Reusable, and bind its state to the RxJs Subject from the injected service using a property named checkBoxState of type boolean, and a method named checkBoxChanged:

The local state of the checkbox will always be updated by the Subject reusableComponentState in State service when a new value is emitted by subscribing to the Subject (serviceService.reusableComponentState.subscribe((bool: boolean): void => { this.checkBoxState = bool; }))

A behaviorSubject will always have a initial value, and it will always return its value to us when we subscribe to it.

It doesn't matter if we loaded the data five minutes ago whenever it subscribes to the behavior subject it's going to get whatever the last data emitted was.

### The checkbox problem:

When we change the route the checkbox value changes in both routes, which means if we have a checkbox in every route we won't be able to change one without changing the second.

The behaviorSubject gets an initial parameter of the value of the checked box but doesn't interfare with the value of the other routes before the first change.
